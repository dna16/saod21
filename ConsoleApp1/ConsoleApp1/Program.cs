﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            MyList<int> lst = new MyList<int>();
            lst.Add(1);
            lst.Add(2);
            lst.Add(3);
            Console.WriteLine(lst.ShowList());
            lst.Insert(2, 5);
            Console.WriteLine(lst.ShowList());
            lst.RemoveAt(2);
            Console.WriteLine(lst.ShowList());
            Console.WriteLine(lst.Last());
            Console.WriteLine(lst.First());
            lst.Clear();
            Console.WriteLine(lst.ShowList());
            Console.ReadLine();
        }
    }
}
