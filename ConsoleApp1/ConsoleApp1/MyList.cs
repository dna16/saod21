﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MyList<T>
    {
        T[] data = new T[1];
        int n = 0;
        public void Add(T name)
        {
            n++;
            Array.Resize(ref data, n+1);
            data[n-1] = name;
        }
        public void Insert(int ind,T name)
        {
            for (int i = n-1;i >= ind;i--)
                data[i+1] = data[i];
            data[ind] = name;
            n++;
            Array.Resize(ref data, n+1);
        }
        public void RemoveAt(int ind)
        {
            for (int i = ind; n > i; i++)
                data[i] = data[i + 1];
            n--;
            Array.Resize(ref data, n + 1);
        }
        public T Last()
        {
            return data[n-1];
        }
        public T First()
        {
            return data[0];
        }
        public void Clear()
        {
            for (int i = 0; n > i; i++)
                data[i] = default(T);
            n = 0;
        }
        public int Size()
        {
            return 0;
        }
        public string ShowList()
        {
            string str = "";
            for (int i = 0; n > i; i++)
            {
                str += Convert.ToString(data[i]) + " ";
            }
            return str;
        }
    }
}
